"use strict";
exports.__esModule = true;
var stringify = require("fast-stable-stringify");
var core_1 = require("@shardus/core");
var crypto = require("@shardus/crypto-utils");
// TODO
// * Include diffing algorithm so we only call set on state that has changed.
//   This would make the app a lot more efficient over the network,
//   however in a sharded network we wouldn't always have access to every slice
//   of the user's state.
crypto.init('abcd123abcd123abcdef3123456e5083934424abcfab9eee8765423111111111');
var GLOBAL_ACCOUNT_ADDRESS = 'a'.repeat(64);
var createAccount = function (accountNumber, state, stateKey) {
    return {
        id: accountNumber,
        timestamp: Date.now(),
        state: state,
        stateKey: stateKey
    };
};
// We need a stable way to derive the accountId from the object key aka stateKey
// so we don't create new accounts every time our user makes a save to the db.
// Also include an optional prefix to tack onto the beginning of the result,
// which will allow the colocation of data.
var objectKeyToAccountId = function (key, prefix) {
    var hash = crypto.hash(key);
    if (prefix) {
        hash = hash.slice(prefix.length);
        hash = prefix + hash;
    }
    return hash;
};
var objectToAccounts = function (object, colocations) {
    var accounts = {};
    for (var key in object) {
        var accountId = objectKeyToAccountId(key, colocations[key]);
        accounts[accountId] = createAccount(accountId, object[key], key);
    }
    return accounts;
};
var accountsToObject = function (accounts) {
    var object = {};
    for (var accountId in accounts) {
        var account = accounts[accountId];
        object[account.stateKey] = account.state;
    }
    return object;
};
// See the idea here is to just have a single account with all the state.
function shardusDB() {
    var shardus = (0, core_1.shardusFactory)({
        server: {
            p2p: {
                minNodesToAllowTxs: 1,
                cycleDuration: 5,
                queryDelay: 5
            }
        }
    });
    var hashAccount = function (account) { return crypto.hashObj(account); };
    var hashTx = function (tx) { return crypto.hashObj(tx); };
    // Database right here boys and girls
    var accounts = {};
    var globalState = null; // TODO I need a primer on global account usage, then design this
    var set = function (state) {
        var txAccounts = objectToAccounts(state, db.colocations);
        var tx = {
            timestamp: Date.now(),
            accounts: txAccounts
        };
        return shardus.put(tx);
    };
    var get = function () {
        return accountsToObject(accounts);
    };
    var setGlobal = function (gs) {
        globalState = gs;
        return {
            success: true,
            reason: 'Couldnt fail!'
        };
        // return shardus.setGlobal(
        //   GLOBAL_ACCOUNT_ADDRESS,
        //   globalState,
        //   Date.now(),
        //   GLOBAL_ACCOUNT_ADDRESS
        // )
    };
    var getGlobal = function () {
        return globalState;
    };
    shardus.setup({
        validate: function (tx) {
            return {
                success: true,
                reason: 'Because this is an internal design consideration of shardusDB. :)'
            };
        },
        apply: function (tx, wrappedAccounts) {
            for (var accountId in tx.accounts) {
                var txAccount = tx.accounts[accountId];
                var account = wrappedAccounts[accountId].data;
                account.state = txAccount.state;
                account.stateKey = txAccount.stateKey;
                account.timestamp = tx.timestamp;
            }
            return shardus.createApplyResponse(hashTx(tx), tx.timestamp);
        },
        crack: function (tx) {
            var keys = Object.keys(tx.accounts);
            return {
                id: hashTx(tx),
                timestamp: tx.timestamp,
                keys: {
                    sourceKeys: keys,
                    targetKeys: keys,
                    allKeys: keys,
                    timestamp: tx.timestamp
                }
            };
        },
        setAccountData: function (accountsToSet) {
            accountsToSet.forEach(function (account) { return accounts[account.id] = account; });
        },
        resetAccountData: function (accountBackupCopies) {
            this.setAccountData(accountBackupCopies);
        },
        deleteAccountData: function (addressList) {
            for (var _i = 0, addressList_1 = addressList; _i < addressList_1.length; _i++) {
                var address = addressList_1[_i];
                delete accounts[address];
            }
        },
        deleteLocalAccountData: function () {
            for (var id in accounts)
                delete accounts[id];
        },
        updateAccountFull: function (wrappedAccount, localCache, applyResponse) {
            var accountId = wrappedAccount.accountId, accountCreated = wrappedAccount.accountCreated, updatedAccount = wrappedAccount.data;
            var ogAccount = accounts[accountId];
            var hashBefore = ogAccount ? hashAccount(ogAccount) : '';
            var hashAfter = hashAccount(updatedAccount);
            accounts[accountId] = updatedAccount;
            shardus.applyResponseAddState(applyResponse, updatedAccount, localCache, accountId, applyResponse.txId, applyResponse.txTimestamp, hashBefore, hashAfter, accountCreated);
        },
        updateAccountPartial: function (wrappedData, localCache, applyResponse) {
            this.updateAccountFull(wrappedData, localCache, applyResponse);
        },
        getRelevantData: function (accountId, tx) {
            var account = accounts[accountId];
            var accountCreated = false;
            var txAccount = tx.accounts[accountId];
            if (!account) {
                account = createAccount(accountId, txAccount.state, txAccount.stateKey);
                accountCreated = true;
            }
            var accountHash = hashAccount(account);
            var wrappedAccount = shardus.createWrappedResponse(accountId, accountCreated, accountHash, account.timestamp, account);
            return wrappedAccount;
        },
        getAccountData: function (accountStart, accountEnd, maxRecords) {
            var wrappedAccounts = [];
            // We'll parse these for later numerical comparisons
            var start = parseInt(accountStart, 16);
            var end = parseInt(accountEnd, 16);
            for (var _i = 0, _a = Object.values(accounts); _i < _a.length; _i++) {
                var account = _a[_i];
                var accountId = parseInt(account.id, 16);
                if (accountId < start || accountId > end)
                    continue;
                var wrappedAccount = shardus.createWrappedResponse(accountId, false, hashAccount(account), account.timestamp, account);
                wrappedAccounts.push(wrappedAccount);
                if (wrappedAccounts.length >= maxRecords)
                    return wrappedAccounts;
            }
            return wrappedAccounts;
        },
        getAccountDataByRange: function (accountStart, accountEnd, tStart, tEnd, maxRecords) {
            var wrappedAccounts = [];
            var start = parseInt(accountStart, 16);
            var end = parseInt(accountEnd, 16);
            for (var _i = 0, _a = Object.values(accounts); _i < _a.length; _i++) {
                var account = _a[_i];
                // Skip if not in account id range
                var id = parseInt(account.id, 16);
                if (id < start || id > end)
                    continue;
                // Skip if not in timestamp range
                var timestamp = account.timestamp;
                if (timestamp < tStart || timestamp > tEnd)
                    continue;
                var wrappedAccount = shardus.createWrappedResponse(account.id, false, hashAccount(account), account.timestamp, account);
                wrappedAccounts.push(wrappedAccount);
                // Return results early if maxRecords reached
                if (wrappedAccounts.length >= maxRecords)
                    return wrappedAccounts;
            }
            return wrappedAccounts;
        },
        getAccountDataByList: function (addressList) {
            var wrappedAccounts = [];
            for (var _i = 0, addressList_2 = addressList; _i < addressList_2.length; _i++) {
                var address = addressList_2[_i];
                var account = accounts[address];
                if (!account)
                    continue;
                var wrappedAccount = shardus.createWrappedResponse(account.id, false, hashAccount(account), account.timestamp, account);
                wrappedAccounts.push(wrappedAccount);
            }
            return wrappedAccounts;
        },
        calculateAccountHash: function (account) {
            return hashAccount(account);
        },
        // This allows you to serialize your account into a string for later viewing
        // within the logs.
        getAccountDebugValue: function (wrappedAccount) {
            return "".concat(stringify(wrappedAccount.data));
        },
        close: function () { console.log('Shutting down...'); }
    });
    shardus.registerExceptionHandler();
    shardus.start();
    var db = {
        get: get,
        set: set,
        getGlobal: getGlobal,
        setGlobal: setGlobal,
        colocations: {},
        shardus: shardus,
        crypto: crypto,
        _accounts: accounts
    };
    return db;
}
exports["default"] = shardusDB;
